This is intended as an advanced HoloViews tutorial. I tried to reproduce one [publication figure](https://www.nature.com/articles/s41598-023-41606-3) of Ilke Albar et al. (originally created with matplotlib), using Postopus and HoloViews (Warning: the data related with this project is over 200 GB in size).

In the following I will list the steps I used for the creation of this folder:

* Open a terminal and establish an SSH connection to the ada cluster: `ssh -L 33334:localhost:33334 USERNAME@ada01.bc.rzg.mpg.de -N`

* Load the Anaconda module: 

  `module load anaconda/3/2021.11`

* Create a conda environment with Python 3.9.
* Install the recommended version of postopus: 

  `pip install -r requirements.txt`

* Start a Jupyter notebook with no browser on port 33334: 

  `jupyter notebook --no-browser --port 33334`

* Try to reproduce the plots of Ilke's original notebooks: under 

  `original_notebooks: plane_efield_plotter_4kevin_original.ipynb`

   and 

  `frame_induced_plotter_forkevin_original.ipynb`

* The actual reproduction is in `final_notebook/case_study_all_final.ipynb`. The data associated with it can be downloaded under https://keeper.mpdl.mpg.de/d/8303c634bfd74c53ad19/

* In `exploration_notebook/case_study_all_in_one_exploration.ipynb`, I demonstrated/simulated how to handle large datasets (even though I used smaller datasets than in the actual reproduction). For more details, see the notebooks.
  The data associated with it can be downloaded under https://keeper.mpdl.mpg.de/d/b37a39d8d3754d6ebd72/



* Note: When I reproduced the notebooks  `nestedRuns` was not yet part of postopus, but would have come handy in certain cases.
* Note2: In order to have interactive slider plots in HTML files, you need to render them as `holoviews.Holomaps`. Please note that to interact with `holoviews.Dynamicmaps`, you will need to run the Jupyter notebooks.


