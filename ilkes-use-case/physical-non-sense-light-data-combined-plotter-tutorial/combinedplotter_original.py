import numpy as np
import matplotlib.pyplot as plt
import os
from scipy import constants,integrate
# import xarray
autos = constants.physical_constants['atomic unit of time'][0] # seconds per a.u. of time
from matplotlib.gridspec import GridSpec
c = 137  ##(au)
root_oct = './'

coef = 4054.0
au_to_nm = 0.0529177249
offset = -12516.9279803-10*coef    # already in nm
thickness =  1600   #nm
x_spiral = [(offset)*au_to_nm,offset*au_to_nm + thickness]

filename = 'e_field-x.z=0'       # str(sys.argv[1])
filename2 = 'e_field-x.x=0,y=0'

try:
    e_field_oct = np.genfromtxt(root_oct+"Maxwell/output_iter/td.0000000/"+filename)
except IOError:
    print('Not run in the proper directory. There should be a Maxwell/output_iter directory and '+filename+' files')
    sys.exit()
npoints = int(np.sqrt(e_field_oct[:, 2].shape[0]))

try:
    e_field_oct2 = np.genfromtxt(root_oct+"Maxwell/output_iter/td.0000000/"+filename2)
except IOError:
    print('Not run in the proper directory. There should be a Maxwell/output_iter directory and '+filename2+' files')
    sys.exit()
proppoints = int(e_field_oct2[:, 1].shape[0])


pos_arr_length = len(e_field_oct2[:,0])
pos_arr = np.zeros(pos_arr_length)
pos_arr[:] = e_field_oct2[:,0]*au_to_nm #convert to nm

with open('exec/parser.log', 'r') as ff:
    lines = ff.readlines()
    for line in lines:
        if 'TDTimeStep' in line:
            dt = float(line.split()[-1])

xarr = np.reshape(e_field_oct[:, 0], (npoints, npoints))
yarr = np.reshape(e_field_oct[:, 1], (npoints, npoints))
xs = xarr[:, 0]
ys = yarr[0, :]
tdDirs = os.listdir(root_oct+'Maxwell/output_iter')
tdDirs.sort()
nframes = int(len(tdDirs))

try:
    outputinterv = int(tdDirs[1][-7:])
except ValueError:
    outputinterv = 1
    
nwrite = nframes #min(nframes, 10)
timeiters = list(range(0, nwrite*outputinterv, outputinterv))
efield_all_timesteps = np.zeros((proppoints,nwrite))
efield = np.zeros((nwrite, npoints, npoints))

#read the fields
for idx, tidx in enumerate(timeiters):   #how far linear medium goes
   ## root_oct = './Maxwell/output_iter/td.{}/'.format(str(tidx).zfill(7))
   ##e_field_oct = np.genfromtxt(root_oct+"Maxwell/"+filename)
    filepath = root_oct+'Maxwell/output_iter/td.{}/'.format(str(tidx).zfill(7))
    e_field_oct = np.genfromtxt(filepath+filename)
    e_field_onedim = np.genfromtxt(filepath+filename2)
    field = e_field_oct[:, 2]
    npoints = int(np.sqrt(field.shape[0]))
    efield[idx, :, :] = np.reshape(field, (npoints, npoints))
    efield_all_timesteps[:, idx] = e_field_onedim[:,1]
    

vmax =  5e-5  #max(abs(efield.max()), abs(efield.min()))
vmax2 = max(abs(efield_all_timesteps.max()), abs(efield_all_timesteps.min()))
axlim = au_to_nm*xs.max()  #conv to nm 
xs_nm = au_to_nm*xs
ys_nm = au_to_nm*ys
y1lim = 5e-5
gs_kw = dict(width_ratios=[1.5], height_ratios=[1, 4])

if not os.path.isdir('combinedrefplots'): os.mkdir('combinedrefplots')
# plot part by part
for idx, tidx in enumerate(timeiters):
    fig , axes = plt.subplots(
        nrows=2, ncols=1, sharex=False, sharey=False, 
        gridspec_kw=gs_kw ,figsize=(15.0, 15.0)
        )
    fig.suptitle("$E_{x}$ for RCP Inc. Pulse  \n Propagation Time : "+ format(tidx*dt*0.02418884327,".2f") #  1 Atomic time unit = 0.024 188 842 54 Femtoseconds [fs]  
                 + ' fs', fontsize=25)
    for xc in x_spiral:
        axes[0].axvline(x=xc,color='r', label='Spiral')
    axes[0].plot(pos_arr,efield_all_timesteps[:,idx])
    axes[0].set_ylim(-vmax,+vmax)
    axes[0].axvspan(x_spiral[0], x_spiral[1], color='r', alpha=0.5)
    axes[0].set_ylim(-y1lim,y1lim)
    axes[0].set_xlabel('z axis (nm)', fontsize=16)
    axes[0].set_ylabel('$E_{x}$ (au)', fontsize=16)
    axes[0].set_title('$E_{x}$ field through propagation axis ', fontsize=20)
    ax3 = axes[1].imshow(efield[idx,:,:].T, aspect = 'auto', cmap=plt.cm.seismic, \
                 extent=[xs_nm.min(), xs_nm.max(), ys_nm.max(), ys_nm.min()], vmax=vmax, vmin=-vmax)
    axes[1].set_xlim(-axlim, axlim)
    axes[1].set_ylim(-axlim, axlim)
    axes[1].set_title('$E_{x}$ field $Z=0$ Plane', fontsize=20)
    axes[1].set_xlabel('x axis (nm)', fontsize=16)
    axes[1].set_ylabel('y axis (nm)', fontsize=16)
    fig.colorbar(ax3, ax=axes[1])
    fig.savefig('combinedrefplots/efieldx_{}.png'.format(str(tidx).zfill(7)), bbox_inches='tight')
    plt.clf()  #close later
    # plt.close(fig)

