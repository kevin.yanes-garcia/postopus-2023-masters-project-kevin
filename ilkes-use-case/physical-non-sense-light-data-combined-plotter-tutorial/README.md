This folder contains a tutorial about the interplay of Postopus and HoloViews in a step-by-step manner (the data is physically meaningless). 
* The data can be downloaded under https://keeper.mpdl.mpg.de/f/e3b4887810de475086ea/ 
* activate a virtual environment
* `pip install -r requirements.txt`
* The data, stored in `archimiedean_spiral.zip`, needs to be decompressed.
* Note: The ata is physically meaningless, this folder is just to show the postopus+holoviews capabilities in a step-by-step guide.
* Note2: In order to have interactive slider plots in HTML files, you need to render them as `holoviews.Holomaps`. Please note that to interact with `holoviews.Dynamicmaps`, you will need to run the Jupyter notebooks.


This tutorial was created during the course of my master's thesis. I tried to reproduce the results (stored in `combinedrefplots`, generated by the script `combinedredplotter_original.py`) in the notebook (`use_case_combined_plotter.ipynb`) using Postopus.

