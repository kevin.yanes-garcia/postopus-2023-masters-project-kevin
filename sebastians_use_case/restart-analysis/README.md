* The (incomplete) data can be downloaded under https://keeper.mpdl.mpg.de/f/7633e78a8c0f4a109c45/


In the following I will list the steps I used for the creation of this folder:
* Activate a virtual environment.
* Install the recommended version of postopus: `pip install -r requirements.txt`

* Note: The restart analysis was an interactive and iterative debugging process. As a result, some data, such as the netcdf and cube data, are missing. This is why some cells in the notebook (stored within the zip) used for reproducing the analysis are failing due to missing data. Fortunately, Sebastian stored the original notebook and the HTML output. These are labeled as "filename_original".
* Note2: In order to have interactive slider plots in HTML files, you need to render them as `holoviews.Holomaps`. Please note that to interact with `holoviews.Dynamicmaps`, you will need to run the Jupyter notebooks.




