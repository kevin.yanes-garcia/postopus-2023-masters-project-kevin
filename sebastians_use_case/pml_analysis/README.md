* The data can be downloaded under https://keeper.mpdl.mpg.de/f/1f77e498c3b6441c8293/.

In the following I will list the steps I used for the creation of this folder:
* Activate a virtual environment
* `pip install -r requirements.txt`
* Execute the cells in the notebook, after decompressing the data files.
* Note: In order to have interactive slider plots in HTML files, you need to render them as `holoviews.Holomaps`. Please note that to interact with `holoviews.Dynamicmaps`, you will need to run the Jupyter notebooks.


