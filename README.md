Use cases that were handled in my master's thesis that may be useful as application example tutorials.

Data and interview videos are stored in Keeper in a folder with the same name as this repository. For the videos ask Professor Fangohr.

- Ilke Albar's use case folder: https://keeper.mpdl.mpg.de/d/8ebd1e2ccc90489884f0/
- Sebastian Ohlmann's use case folder: https://keeper.mpdl.mpg.de/d/2505e315bb22410ebaa4/